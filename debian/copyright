Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-opcua
Upstream-Contact: Olivier Roulet-Dubonnet <olivier.roulet@gmail.com>
Source: https://github.com/FreeOpcUa/python-opcua

Files: *
Copyright: 2016-2018, Olivier Roulet-Dubonnet <olivier.roulet@gmail.com>
License: LGPL-3+

Files: schemas/OPCBinarySchema.xsd
       schemas/Opc.Ua.Adi.*
       schemas/Opc.Ua.Classes.cs
       schemas/Opc.Ua.DataTypes.cs
       schemas/Opc.Ua.Di.*
       schemas/Opc.Ua.NodeSet*
       schemas/Opc.Ua.PredefinedNodes.xml
       schemas/Opc.Ua.Services.wsdl
       schemas/Opc.Ua.Types.bsd
       schemas/SecuredApplication.xsd
       schemas/UANodeSet.xsd
Copyright: 2005-2019, The OPC Foundation Inc.
License: Expat

Files: debian/*
Copyright: 2016, W. Martin Borgert <debacle@debian.org>
License: LGPL-3+

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License,
 Version 3 can be found in the file /usr/share/common-licenses/LGPL-3

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
